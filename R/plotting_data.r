# load packages and data
source("R/load_packages.r")
source("R/data_analysis.r")

p1 = ggplot(diamonds, aes(x = carat, y = price, col = cut)) +
  geom_point() +
  labs(x = "Carat", y = "Price (£)", 
       title = "Scatterplot of Carat vs Price for a sample of the Diamonds dataset")
p1

# add violin of price, split by cut
p2 = ggplot(diamonds, aes(x = cut, y = price)) +
  geom_violin()
p2

# create Plots directory if it doesn't already exist
if (!dir.exists("Plots")) {
  dir.create("Plots")
}

# save results for use later (but ignored from git)
ggsave(filename = "Plots/scatterplot.pdf", p1, device = "pdf")

# testing using ggridges
ggplot(diamonds, aes(x = price, y = cut)) +
  geom_density_ridges()
