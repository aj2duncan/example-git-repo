# check if packages installed
pckgs_needed <- c("ggplot2", "dplyr", "tidyr", "ggridges")
pckgs_to_install <- pckgs_needed[!(pckgs_needed 
                                   %in% installed.packages()[,"Package"])]
if (length(pckgs_to_install)) { # will be zero if all installed
  install.packages(new.packages, dependecies = TRUE)
}

# load packages
library(dplyr)
library(tidyr)
library(ggplot2)
library(ggridges)