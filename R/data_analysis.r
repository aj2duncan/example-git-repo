# source packages file
source("R/load_packages.r")

diamonds <- sample_n(diamonds, 1000)

# some summary calculations
diamonds %>% 
  group_by(cut) %>%
  summarise(av_price = mean(price, na.rm = TRUE))

